# planning-mujoco-ompl

![alt text](/other/render.gif)

This repository allows you to use various geometric planners from OMPL in scenes from MuJoCo, using MuJoCo to check for collisions. The repository is a kind of an interface between these libraries, allowing them to be used together

## Requirements
- [MuJoCo](https://mujoco.org/) version 2.3.0 or greater 
- [OMPL](https://ompl.kavrakilab.org/)
- [GLFW](https://www.glfw.org/)
- [yaml-cpp](https://github.com/jbeder/yaml-cpp)
- Boost
## Build

To build repository on Linux you should perform those commands

```
mkdir build
cmake ..
make
```

## Usage 
To plan something, you need to run following command
```
./plan /path/to/mujoco/scene.xml /path/to/problem_definition.yaml planning_time 
```

After that, you will receive a ```plan.out``` file that will contain the found path in the form of intermediate states. To display the path, run the command
```
./render /path/to/mujoco/scene.xml plan.out
```
The task for planning should be done according to the [template](problems/problem_template.yaml)

Examples of different scenes and problems are located in [problems](problems/) folder

The model of the robot we used is in the [model](model/) folder
## Acknowledgments
This project is partially based on the code from [this](https://github.com/mpflueger/mujoco-ompl) repository

The robot model is taken from [here](https://github.com/deepmind/mujoco_menagerie)