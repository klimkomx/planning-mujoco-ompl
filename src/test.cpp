#include <memory>

#include <ompl/geometric/planners/rrt/RRTstar.h>
#include <ompl/base/spaces/SE3StateSpace.h>

namespace ob = ompl::base;
namespace og = ompl::geometric;

bool isStateValid(const ob::State *state) {
    return true;
}

void plan() {
    auto space(std::make_shared<ob::SE3StateSpace>());
    
    //установление границ
    ob::RealVectorBounds bounds(3);
    bounds.setLow(-1);
    bounds.setHigh(1);
 
    space->setBounds(bounds);

    //
    auto si(std::make_shared<ob::SpaceInformation>(space));

    si->setStateValidityChecker(isStateValid);

    ob::ScopedState<> start(space);
    start.random();

    ob::ScopedState<> goal(space);
    goal.random();

    auto pdef(std::make_shared<ob::ProblemDefinition>(si));
    pdef->setStartAndGoalStates(start, goal);

    auto planner(std::make_shared<og::RRTstar>(si));

    planner->setProblemDefinition(pdef);
    planner->setup();

    ob::PlannerStatus is_solved = planner->ob::Planner::solve(10.0);

    if (is_solved) {
        std::cout << "Found solution:" << std::endl;
 
        ob::PathPtr path = pdef->getSolutionPath();
        path->print(std::cout);
    } else {
        
    }
}

int main() {
    plan();
}