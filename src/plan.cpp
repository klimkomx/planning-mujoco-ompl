// Plan for a MuJoCo environment with OMPL

#include <cstdlib>
#include <fstream>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>

#include <ompl/geometric/SimpleSetup.h>


#include <ompl/geometric/planners/rrt/RRTstar.h>
#include <ompl/geometric/planners/rrt/InformedRRTstar.h>
#include <ompl/geometric/planners/rrt/RRT.h>
#include <ompl/geometric/planners/rrt/RRTConnect.h>

#include <ompl/geometric/planners/prm/PRM.h>
#include <ompl/geometric/planners/informedtrees/BITstar.h>
#include <ompl/geometric/planners/fmt/FMT.h>


#include <yaml-cpp/yaml.h>
#include "mujoco_wrapper.h"
#include "mujoco_ompl_interface.h"

namespace ob = ompl::base;
namespace og = ompl::geometric;

using std::vector, std::string, std::cin, std::cout, std::cerr;
using namespace std;


int main(int argc, char** argv) {
    std::string xml_filename = "";
    std::string prob_config_filename = "";
    if (argc >= 3) {
        xml_filename = argv[1];
        prob_config_filename = argv[2];
    } else {
        std::cerr << "Format: plan_ompl_kinematic <MuJoCo XML config> <yaml problem spec> [time limit]" << endl;
        return -1;
    }

    // Optional time limit
    double timelimit = 1.0;
    if (argc >= 4) {
        stringstream ss;
        ss << argv[3];
        ss >> timelimit;
    }

    // Load yaml information
    //   This should contain instructions on how to setup the planning problem
    std::vector<double> start_vec;
    std::vector<double> goal_vec;
    std::string planner_name;
    if (prob_config_filename != "") {
        YAML::Node node = YAML::LoadFile(prob_config_filename);

        // Copy variables
        if (node["start"]) {
            start_vec = node["start"].as<std::vector<double> >();
        }
        if (node["goal"]) {
            goal_vec = node["goal"].as<std::vector<double> >();
        }
        planner_name = node["planner"].as<std::string>();
    }



    // Create MuJoCo Object
    std::string mjkey_filename = strcat(getenv("HOME"), "/.mujoco/mjkey.txt");
    auto mj(std::make_shared<MuJoCo>(mjkey_filename));

    // Get xml file name
    if (xml_filename.find(".xml") == std::string::npos) {
        std::cerr << "XML model file is required" << std::endl;
        return -1;
    }

    // Load Model
    std::cout << "Loading MuJoCo config from: " << xml_filename << std::endl;
    if (!mj->loadXML(xml_filename)) {
        std::cerr << "Could not load XML model file" << std::endl;
        return -1;
    }

    // Make data
    if (!mj->makeData()) {
        std::cerr << "Could not allocate mjData" << std::endl;
        return -1;
    }

    // Setup OMPL environment
    auto si = MjOmpl::createSpaceInformationKinematic(mj->m);
    si->setStateValidityChecker(std::make_shared<MjOmpl::MujocoStateValidityChecker>(si, mj, false));

    // Create planner

    auto rrtstar_planner(std::make_shared<og::RRTstar>(si));
    auto rrt_planner(std::make_shared<og::RRT>(si));
    auto infrrtstar_planner(std::make_shared<og::InformedRRTstar>(si));
    auto prm_planner(std::make_shared<og::PRM>(si));
    auto bitstar_planner(std::make_shared<og::BITstar>(si));
    auto fmt_planner(std::make_shared<og::FMT>(si));

    rrtstar_planner->setRange(0.1);
    // bitstar_planner->setRange(0.1);

    si->setup();

    // Create a SimpleSetup object
    og::SimpleSetup ss(si);

    if (planner_name == "rrt_star") {
        ss.setPlanner(rrtstar_planner);
    } else if (planner_name == "bit_star") {
        ss.setPlanner(bitstar_planner);
    } else if (planner_name == "fmt") {
        ss.setPlanner(fmt_planner);
    } else if (planner_name == "rrt") {
        rrt_planner -> setRange(0.1);
        ss.setPlanner(rrt_planner);
    } else if (planner_name == "informed_rrt_star") {
        ss.setPlanner(infrrtstar_planner);
    } else if (planner_name == "prm") {
        ss.setPlanner(prm_planner);
    }

    // Set start and goal states
    ob::ScopedState<> start_ss(ss.getStateSpace());
    for(int i=0; i < start_vec.size(); i++) {
        start_ss[i] = start_vec[i];
    }
    ob::ScopedState<> goal_ss(ss.getStateSpace());
    for(int i=0; i < goal_vec.size(); i++) {
        goal_ss[i] = goal_vec[i];
    }

    ss.setStartAndGoalStates(start_ss, goal_ss);

    // Call the planner
    ob::PlannerStatus solved = ss.solve(timelimit);

    if (solved == ob::PlannerStatus::StatusType::EXACT_SOLUTION) {
        std::cout << "Found exact solution"  << std::endl;
        std::cout << "Path length : " << path.length() << std::endl;

        std::ofstream out_file;
        out_file.open("plan.out");
        auto path = ss.getSolutionPath();
        path.interpolate();
        path.printAsMatrix(out_file);
        out_file.close();
    } else if (solved) {
        std::cout << "Found approximate solution"  << std::endl;
        std::cout << "Path length : " << path.length() << std::endl;

        std::ofstream out_file;
        out_file.open("plan.out");
        auto path = ss.getSolutionPath();
        path.interpolate();
        path.printAsMatrix(out_file);
        out_file.close();

    } else {
        std::cout << "No solution found" << std::endl;
    }

    return 0;
}
