
#include <ompl/base/StateSpace.h>

#include "compound_state_projector.h"

#include "mujoco_ompl_interface.h"

namespace ob = ompl::base;
namespace oc = ompl::control;
using namespace std;

namespace MjOmpl {

void readOmplStateKinematic(
        const vector<double>& x,
        const ob::SpaceInformation* si,
        ob::State* state) {
    // Vector format: [state]

    // assert(si->getStateSpace()->isCompound());
    // auto css(si->getStateSpace()->as<ob::CompoundStateSpace>());
    auto css(si->getStateSpace()->as<ob::RealVectorStateSpace>());

    // Make sure the data vector is the right size
    // cout << "x: " << x.size() << " css: " << css->getDimension()
    //      << " rvcs: " << rvcs->getDimension() << endl;
    assert(x.size() == css->getDimension());

    for(size_t j=0; j < x.size(); j++) {
        state->as<ob::RealVectorStateSpace::StateType>()->values[j] = x[j];
    }
}

std::shared_ptr<ompl::base::RealVectorStateSpace>
makeRealVectorStateSpace(const mjModel *m, bool include_velocity) {
    auto joints = getJointInfo(m);

    uint dim;
    if (include_velocity) {
        dim = 2 * joints.size();
    } else {
        dim = joints.size();
    }

    auto space(make_shared<ob::RealVectorStateSpace>(dim));

    ob::RealVectorBounds bounds(dim);
    int i = 0;
    for(const auto& joint : joints) {
        bounds.setLow(i, joint.range[0]);
        bounds.setHigh(i, joint.range[1]);
        i++;
    }
    space->setBounds(bounds);
    return space;
}


shared_ptr<ob::SpaceInformation> createSpaceInformationKinematic(
    const mjModel* m) {
    auto space = makeRealVectorStateSpace(m, false);
    auto si(make_shared<ob::SpaceInformation>(space));
    return si;
}


void copyOmplStateToMujoco(
        const ob::State* state,
        const ob::SpaceInformation* si,
        const mjModel* m,
        mjData* d,
        bool useVelocities) {
    size_t n;
    auto css(si->getStateSpace()->as<ob::RealVectorStateSpace>());

    n = css->getDimension();

    for(size_t i=0; i < n; i++) {
        d->qpos[i] = state->as<ob::RealVectorStateSpace::StateType>()->values[i];
    }
}


void copyMujocoStateToOmpl(
        const mjModel* m,
        const mjData* d,
        const ob::SpaceInformation* si,
        ob::State* state,
        bool useVelocities) {
    size_t n;
    auto css(si->getStateSpace()->as<ob::RealVectorStateSpace>());

    n = css->getDimension();

    // Copy vector
    for(size_t j=0; j < n; j++) {
        state->as<ob::RealVectorStateSpace::StateType>()->values[j] = d->qpos[j];
    }
}


void copyOmplControlToMujoco(
        const oc::RealVectorControlSpace::ControlType* control,
        const oc::SpaceInformation* si,
        const mjModel* m,
        mjData* d) {
    int dim = si->getControlSpace()->as<oc::RealVectorControlSpace>()
        ->getDimension();
    if (dim != m->nu) {
        throw invalid_argument(
            "SpaceInformation and mjModel do not match in control dim");
    }

    for(size_t i=0; i < dim; i++) {
        d->ctrl[i] = control->values[i];
    }
}

std::shared_ptr<ompl::control::SpaceInformation>
createSpaceInformation(const mjModel* m);

void copySO3State(
        const ob::SO3StateSpace::StateType* state,
        double* data) {
    data[0] = state->w;
    data[1] = state->x;
    data[2] = state->y;
    data[3] = state->z;
}


void copySO3State(
        const double* data,
        ob::SO3StateSpace::StateType* state) {
    state->w = data[0];
    state->x = data[1];
    state->y = data[2];
    state->z = data[3];
}


void copySE3State(
        const ob::SE3StateSpace::StateType* state,
        double* data) {
    data[0] = state->getX();
    data[1] = state->getY();
    data[2] = state->getZ();
    copySO3State(&state->rotation(), data + 3);
}


void copySE3State(
        const double* data,
        ob::SE3StateSpace::StateType* state) {
    state->setX(data[0]);
    state->setY(data[1]);
    state->setZ(data[2]);
    copySO3State(data + 3, &state->rotation());
}

void MujocoStatePropagator::propagate( const ob::State* state,
                                       const oc::Control* control,
                                       double duration,
                                       ob::State* result) const {
    //cout << " -- propagate asked for a timestep of: " << duration << endl;

    mj_lock.lock();

    copyOmplStateToMujoco(state->as<ob::CompoundState>(), si_, mj->m, mj->d);
    copyOmplControlToMujoco(
        control->as<oc::RealVectorControlSpace::ControlType>(),
        si_,
        mj->m,
        mj->d);

    mj->sim_duration(duration);

    // Copy result to ob::State*
    copyMujocoStateToOmpl(mj->m, mj->d, si_, result->as<ob::CompoundState>());

    mj_lock.unlock();
}

bool MujocoStateValidityChecker::isValid(const ompl::base::State *state) const {
    mj_lock.lock();
    copyOmplStateToMujoco(
            state,
            si_,
            mj->m,
            mj->d,
            useVelocities);

    mj_fwdPosition(mj->m, mj->d);
    int ncon = mj->d->ncon;
    mj_lock.unlock();
    return ncon==0;
}

} // MjOmpl namespace
